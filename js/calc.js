











//window.onload = function() {
//    clickFunc();
//    main();
//};

window.setAppName = function (name) {
    var appname = document.getElementById("appname");
    appname.innerHTML = name;
}

function clickFunc() {
    var container = document.getElementById("container");
    var calc = document.getElementById("calc"),
        equals = document.getElementById("equals"), //等于号
        remove = document.getElementById("remove"); //删除符号

    /***********点击键盘***********/
    var keyBorders = document.querySelectorAll("#bottom span"),
        express = document.getElementById("express"), //计算表达式
        res = document.getElementById("res"), //输出结果
        keyBorde = null; //键盘
    var preKey = ""; //上一次按的键盘
    //符号
    var symbol = { "+": "+", "-": "-", "×": "*", "÷": "/", "%": "%", "=": "=" };

    /***********键盘按钮***********/
    for (var j = 0; j < keyBorders.length; j++) {
        keyBorde = keyBorders[j];

        keyBorde.onclick = function() {
            var number = this.dataset["number"];
            clickNumber(number);
        };
    }

    window.printText = function(val) {
        window.textFlag = 1;
        res.innerHTML = val;
    }

    /**
     * 点击键盘进行输入
     * @param {string} number 输入的内容
     * */
    function clickNumber(number) {
        if (window.textFlag) {
            express.innerHTML = "";
            res.innerHTML = "";
            delete window.textFlag;
        }

        var resVal = res.innerHTML; //结果
        var exp = express.innerHTML; //表达式
        //表达式最后一位的符号
        var expressEndSymbol = exp.substring(exp.length - 1, exp.length);
        //点击的不是删除键和复位键时才能进行输入
        if (number !== "←" || number !== "C") {
            //是否已经存在点了，如果存在那么不能接着输入点号了,且上一个字符不是符号字符
            var hasPoint = (resVal.indexOf('.') !== -1) ? true : false;
            if (hasPoint && number === '.') {
                //上一个字符如果是符号，变成0.xxx形式
                if (symbol[preKey]) {
                    res.innerHTML = "0";
                } else {
                    return false;
                }
            }
            //转换显示符号
            if (isNaN(number)) {
                number = number.replace(/\*/g, "×").replace(/\//g, "÷");
            }
            //如果输入的都是数字，那么当输入达到固定长度时不能再输入了
            if (!symbol[number] && isResOverflow(resVal.length + 1) && !symbol[expressEndSymbol] && exp.length > 0) {
                return false;
            }
            //点击的是符号
            //计算上一次的结果
            if (symbol[number]) {
                //上一次点击的是不是符号键
                if (preKey !== "=" && symbol[preKey]) {
                    express.innerHTML = exp.slice(0, -1) + number;
                } else {
                    if (exp == "") {
                        express.innerHTML = resVal + number;
                    } else {
                        express.innerHTML += resVal + number;
                    }
                    if (symbol[expressEndSymbol]) {
                        exp = express.innerHTML.replace(/×/g, "*").replace(/÷/, "/");
                        res.innerHTML = eval(exp.slice(0, -1));
                    }
                }
            } else {
                //如果首位是符号，0
                if ((symbol[number] || symbol[preKey] || resVal == "0") && number !== '.') {
                    res.innerHTML = "";
                }
                res.innerHTML += number;
            }
            preKey = number;
        }
    }

    /***********相等，计算结果***********/
    equals.onclick = function() {
        calcEques();
    };

    function calcEques() {
        var expVal = express.innerHTML,
            val = "";
        var resVal = res.innerHTML;
        //表达式最后一位的符号
        if (expVal) {
            var expressEndSymbol = expVal.substring(expVal.length - 1, expVal.length);
            try {
                var temp = "";
                if (symbol[expressEndSymbol] == '%') {
                    temp = resVal + "/100";
                } else if (symbol[expressEndSymbol] && resVal) {
                    temp = expVal.replace(/×/g, "*").replace(/÷/, "/");
                    temp = eval(temp.slice(0, -1)) + symbol[expressEndSymbol] + resVal;
                } else {
                    temp = expVal.replace(/×/g, "*").replace(/÷/, "/");
                }
                console.log("express: " + temp);
                val = window.compute(temp); //eval(temp);
		if (val == "")
			throw true;
            } catch (error) {
                val = "<span style='text-align:right;font-size:3.6rem;color:pink'>Erro：计算出错！</span>";
            } finally {
                express.innerHTML = "";
                if (isNaN(val))
                    printText(val);
                else
                    res.innerHTML = parseFloat(val.toFixed(10));;
                preKey = "=";
                isResOverflow(resVal.length);
            }
        }
    }


    /***********复位操作***********/
    var resetBtn = document.getElementById("reset"); //复位按钮
    resetBtn.onclick = function() {
        res.innerHTML = "0";
        express.innerHTML = "";
    };

    /***********减位操作***********/
    remove.onclick = function() {
        var tempRes = res.innerHTML;
        if (tempRes.length > 1) {
            tempRes = tempRes.slice(0, -1);
            res.innerHTML = tempRes;
        } else {
            res.innerHTML = 0;
        }
    };


    /**********自动设置文字大小************/
    function isResOverflow(leng) {
        var calc = document.getElementById("calc");
        var w = calc.style.width || getComputedStyle(calc).width || calc.currentStyle.width;
        w = parseInt(w);

        //判断是否是移动端
        if ((Mybry.browser.versions.android || Mybry.browser.versions.iPhone || Mybry.browser.versions.iPad) && !symbol[preKey]) {
            if (leng > 15) {
                return true;
            }
        } else {
            if (leng > 10) {
                return true;
            }
        }
        return false;
    }
}

clickFunc();
